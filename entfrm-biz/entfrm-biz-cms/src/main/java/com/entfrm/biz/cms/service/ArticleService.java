package com.entfrm.biz.cms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.cms.entity.Article;

/**
 * @author entfrm
 * @date 2020-04-01 10:04:11
 * @description 文章Service接口
 */
public interface ArticleService extends IService<Article> {
}
