package com.entfrm.biz.msg.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.msg.entity.InfoTemplate;

/**
 * @author entfrm
 * @date 2020-05-24 22:26:58
 *
 * @description 消息模板Mapper接口
 */
public interface InfoTemplateMapper extends BaseMapper<InfoTemplate>{

}
